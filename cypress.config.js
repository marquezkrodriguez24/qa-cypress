require("dotenv").config();
const { defineConfig } = require("cypress");
// const getspecFiles = require("./cypress-parallel");

const {
  CYPRESS_HOST_PAGE_WEB_LOCAL,
  CYPRESS_HOST_PAGE_MOD,
  CYPRESS_HOST_API_V1,
  CYPRESS_HOST_API_V2,
  CYPRESS_HOST_API_RESERVE,
  CYPRESS_EMAIL,
  CYPRESS_PASSWORD,
  CYPRESS_STAGING_OR_PRD,
} = process.env;

module.exports = defineConfig({
  video: true,
  defaultCommandTimeout: 10000,
  env: {
    users: [
      {
        email: "kmarquez@zeroq.cl",
        password: "km199717*",
        offices: [
          {
            id: 2494,
            nameOffice: "Kevin QA",
            slug: "kevin-qa",
            nameModule: "Modulo 2", // asignados modules:[{nameModule:"Modulo 2"},{nameModule:"Modulo 3"}]
            ticketAmount: 1,
            filasNumberAttention: 2,
            typeModule: false,
          },
          {
            id: 1777,
            nameOffice: "Demo web - Frontend",
            slug: "demo-web-frontend",
            nameModule: "Kevin Test",
            ticketAmount: 1,
            filasNumberAttention: 2,
            typeModule: true,
          },
        ],
      },
      {
        email: "qatestkm@zeroq.cl",
        password: "Z3r0Q-Q4-2023*/",
        offices: [
          {
            id: 2494,
            nameOffice: "Kevin QA",
            slug: "kevin-qa",
            nameModule: "Modulo 3",
            ticketAmount: 1,
            filasNumberAttention: 2,
            typeModule: false,
          },
          {
            id: 1777,
            nameOffice: "Demo web - Frontend",
            slug: "demo-web-frontend",
            nameModule: "Kevin Test",
            ticketAmount: 1,
            filasNumberAttention: 2,
            typeModule: true,
          },
        ],
      },
      {
        email: "kmarquez0817@gmail.com",
        password: "km199717*",
      },
    ],
    email: CYPRESS_EMAIL,
    password: CYPRESS_PASSWORD,
    pageLocalWeb: `${CYPRESS_HOST_PAGE_WEB_LOCAL}`,
    pageModule: `${CYPRESS_HOST_PAGE_MOD}`,
    stagingOrPrd: CYPRESS_STAGING_OR_PRD,
    questionsForm: [
      { question: "Nombre completo", answer: "kmarquez@zeroq.cl" },
      { question: "E-mail", answer: "kmarquez@hmail.com" },
      { question: "Edad", answer: "25 años" },
    ],
    authAPI_V1:
      "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI0OEhKT0gyOEd3TWtnekVaSFdWc1ZqUFlWVjMzIiwidXNlciI6eyJpZCI6NDY0MzYxNiwidXNlcl9pZCI6NDY0MzYxNiwidWlkIjoiNDhISk9IMjhHd01rZ3pFWkhXVnNWalBZVlYzMyIsIm5hbWUiOiJLZXZpbiBNYXJxdWV6IiwicnV0IjoiOTk5OTk5OTk5IiwidHlwZSI6ImFkbWluIiwiZW1haWwiOiJrbWFycXVlekB6ZXJvcS5jbCIsInByb3ZpZGVyIjoiemVyb3EiLCJtZXRhIjp7Im5hbWUiOiJLZXZpbiBNYXJxdWV6IiwicnV0IjoiOTk5OTk5OTk5IiwidGVtcG9yYXJ5IjpmYWxzZX19LCJ0eXBlIjoiYWRtaW4iLCJpZCI6NDY0MzYxNiwidXNlcl9pZCI6NDY0MzYxNiwiZW1haWwiOiJrbWFycXVlekB6ZXJvcS5jbCIsInByb3ZpZGVyIjoiemVyb3EiLCJpYXQiOjE2NzEwMzQ4NjgsImlzcyI6Ilplcm9RIn0.Kp6exRahJ5F2OhGElD2p5tQViMJM5p9On41tmr_yiflWVmd7dHKU7OjEGs6mVtjuuXIsw8rUlr_JaHtQev_Pvw",

    authAPI_ATT:
      "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI0OEhKT0gyOEd3TWtnekVaSFdWc1ZqUFlWVjMzIiwidXNlciI6eyJpZCI6NDY0MzYxNiwidXNlcl9pZCI6NDY0MzYxNiwidWlkIjoiNDhISk9IMjhHd01rZ3pFWkhXVnNWalBZVlYzMyIsIm5hbWUiOiJLZXZpbiBNYXJxdWV6IiwicnV0IjoiOTk5OTk5OTk5IiwidHlwZSI6ImFkbWluIiwiZW1haWwiOiJrbWFycXVlekB6ZXJvcS5jbCIsInByb3ZpZGVyIjoiemVyb3EiLCJtZXRhIjp7Im5hbWUiOiJLZXZpbiBNYXJxdWV6IiwicnV0IjoiOTk5OTk5OTk5IiwidGVtcG9yYXJ5IjpmYWxzZX19LCJ0eXBlIjoiYWRtaW4iLCJpZCI6NDY0MzYxNiwidXNlcl9pZCI6NDY0MzYxNiwiZW1haWwiOiJrbWFycXVlekB6ZXJvcS5jbCIsInByb3ZpZGVyIjoiemVyb3EiLCJpYXQiOjE2Njg2MDg1NjQsImlzcyI6Ilplcm9RIn0.vyc5pzsSF7QKzeT4t8mfO7o_AG0q6v77vtIMWExhv5PyNaTvkLfCCQFz7BFmE_HUpXogVZxk_PfcZTSVZPFVHg",

    authAPI_COMMAND:
      "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiIySDV3UUlBVzdhVERkblRmNkZmWDhCTmJoSnQyIiwidXNlciI6eyJpZCI6MTI0MSwidXNlcl9pZCI6MTI0MSwidWlkIjoiMkg1d1FJQVc3YVREZG5UZjZGZlg4Qk5iaEp0MiIsIm5hbWUiOiJIZXJ2aXMgUGljaGFyZG8iLCJydXQiOiIyNTgyNDA1My03IiwicGhvbmUiOiIrNTZudWxsIiwidHlwZSI6ImFkbWluIiwiZW1haWwiOiJocGljaGFyZG9AemVyb3EuY2wiLCJwcm92aWRlciI6Inplcm9xIiwibWV0YSI6eyJuYW1lIjoiSGVydmlzIFBpY2hhcmRvIiwicnV0IjoiMjU4MjQwNTMtNyIsInBob25lIjoiKzU2bnVsbCIsInRlbXBvcmFyeSI6ZmFsc2V9fSwidHlwZSI6ImFkbWluIiwiaWQiOjEyNDEsInVzZXJfaWQiOjEyNDEsImVtYWlsIjoiaHBpY2hhcmRvQHplcm9xLmNsIiwicHJvdmlkZXIiOiJ6ZXJvcSIsImlhdCI6MTYzMzAwMzE4NywiaXNzIjoiWmVyb1EifQ.oz7DB1ST-_J2uVttCqpXeoUA4VpVS_bdxQxKnPLim4KYVKdJOMow5LN2A-AlAgp0gHhyQgA-ShJvTN03G7FP-A",

    authAPI_Reservation:
      "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI0OEhKT0gyOEd3TWtnekVaSFdWc1ZqUFlWVjMzIiwidXNlciI6eyJpZCI6NDY0MzYxNiwidXNlcl9pZCI6NDY0MzYxNiwidWlkIjoiNDhISk9IMjhHd01rZ3pFWkhXVnNWalBZVlYzMyIsIm5hbWUiOiJLZXZpbiBNYXJxdWV6IiwicnV0IjoiOTk5OTk5OTk5IiwidHlwZSI6ImFkbWluIiwiZW1haWwiOiJrbWFycXVlekB6ZXJvcS5jbCIsInByb3ZpZGVyIjoiemVyb3EiLCJtZXRhIjp7Im5hbWUiOiJLZXZpbiBNYXJxdWV6IiwicnV0IjoiOTk5OTk5OTk5IiwidGVtcG9yYXJ5IjpmYWxzZX19LCJ0eXBlIjoiYWRtaW4iLCJpZCI6NDY0MzYxNiwidXNlcl9pZCI6NDY0MzYxNiwiZW1haWwiOiJrbWFycXVlekB6ZXJvcS5jbCIsInByb3ZpZGVyIjoiemVyb3EiLCJpYXQiOjE2NzM0NzEzNDQsImlzcyI6Ilplcm9RIn0.E3_y8MfLe9ib47kZnVLeRQ-Xdt07FTnzm8hwMOVZR-5IvQc79OzOBh9S3GdShdM91iDVXUpCcSdx6dxi0D_kpA",

    moduleRow: 2,
    hostV1: `${CYPRESS_HOST_API_V1}`,
    hostV2: `${CYPRESS_HOST_API_V2}`,
    hostReserve: `${CYPRESS_HOST_API_RESERVE}`,
    times: {
      callTicket: 6000,
      modals: 5000,
      recall: 3000,
    },
    reservation: {
      block: {
        countRows: 4,
        countDays: 1,
      },
    },
  },
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      // return require('./cypress/plugins/index.js')(on, config)
      // console.log(config.env);
      // getspecFiles("./cypress/e2e", true);
      // return config;
    },
    // baseUrl: 'http://192.168.1.5:3030/',
    chromeWebSecurity: false,
    // excludeSpecPattern: process.env.CI ? '' : []
  },
});
