// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })



Cypress.Commands.add("setAttentionReasons", () => {
  const attentionReason = cy.get('.attentionReasons > h2').contains('motivos');
  if (attentionReason) {
    cy.get(':nth-child(1) > .check > label').click();
    cy.wait(500)
    cy.get('.btn-primary').click()
    cy.wait(1000)
  }
 })

Cypress.Commands.add("setPauseReasons", () => {
  const reason = cy.get('.lock-screen > :nth-child(1) > .title').contains('motivo');
  if (reason) {
    cy.get('.lock-screen > :nth-child(1) > :nth-child(2)').click();
  } else {
    cy.get('.toggle_attending').click()
  }
 })

Cypress.Commands.add("loginModulo", (eq) => {
  cy.visit('/modulo');
  cy.wait(3000)
  cy.get('input').type("00")
  cy.get('.btn-primary').click()
  // select one modulo
  cy.get('select > option')
  .eq(eq || 1)
  .then(element => cy.get('select').select(element.val()))

  cy.get('.btn').click()
 })

 



