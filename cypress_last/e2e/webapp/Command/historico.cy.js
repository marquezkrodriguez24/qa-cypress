/// <reference types="cypress" />

describe('Historico', () => {
  beforeEach(() => {
    // Cypress starts out with a blank slate for each test
    // so we must tell it to visit our website with the `cy.visit()` command.
    // Since we want to visit the same URL at the start of all our tests,
    // we include it in our beforeEach function so that it runs before each test

  })

  it('ingresar con clave correcta', () => {
    cy.visit('https://zeroq.cl')
    cy.wait(5000)
    // LE doy click al Botón iniciar sesión
    cy.get('body').contains("Iniciar sesión").click({force: true})

    //tipear email
    cy.get('input[name="email"]').type("dsalazar@zeroq.cl")
    cy.get('input[name="password"]').type("Sup3rZ3r0Q!")

    cy.get('body').contains("Iniciar Sesión").click({force: true})

    cy.wait(3000)
    // verificar que se haya logeado
    cy.get('body').contains("Sucursales Favoritas")


    //ir a command

    cy.visit('https://zeroq.cl/command/')
    cy.wait(5000)

    //buscar sucursal

    cy.get('body').contains('input').type("Qa informatica").enter()

    //ingresar a historico
  
    cy.get('body').contains("Historico").click({force:true})

  })
})