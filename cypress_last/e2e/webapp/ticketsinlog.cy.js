describe('Ticket sin log', () => {
    beforeEach(() => {
      // Cypress starts out with a blank slate for each test
      // so we must tell it to visit our website with the `cy.visit()` command.
      // Since we want to visit the same URL at the start of all our tests,
      // we include it in our beforeEach function so that it runs before each test
  
    })
  
    it('ticket sin log', () => {


      cy.visit('https://zeroq.cl/offices/qa-informatica-1')
      cy.wait(5000)
      //busca Botóndemo
   
      cy.get('body').contains("Pre-Ingreso").click({force:true})

      cy.get('body').contains("Unirse a la fila").click({force:true})
      cy.wait(3000)

      cy.get('body').contains("Continuar sin registrarte").click({force:true})

      cy.get('body').contains("Sacar ticket").click({force:true})
  
    })
})