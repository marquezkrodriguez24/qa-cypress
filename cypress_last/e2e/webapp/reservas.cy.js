/// <reference types="cypress" />

describe('Reserva', () => {
    beforeEach(() => {
      // Cypress starts out with a blank slate for each test
      // so we must tell it to visit our website with the `cy.visit()` command.
      // Since we want to visit the same URL at the start of all our tests,
      // we include it in our beforeEach function so that it runs before each test
  
    })

//incio de sesión con toma de reserva

it('ingresar con clave correcta', () => {
    cy.visit('https://zeroq.cl')
    cy.wait(5000)
    // LE doy click al Botón iniciar sesión
    cy.get('body').contains("Iniciar sesión").click({force: true})

    //tipear email
    cy.get('input[name="email"]').type("agente@zeroq.cl")
    cy.get('input[name="password"]').type("Z3r0Q-Q4-2021*/")

    cy.get('body').contains("Iniciar Sesión").click({force: true})
    cy.wait(6000)

    //ir a tomar reserva
  
    //ingresa a la oficina
    cy.visit('https://zeroq.cl/offices/qa-informatica-1')
    cy.wait(5000)

    //toma la reserva
    cy.get('body').contains("Integracion BCI").click({force:true})

    cy.get('body').contains("Reservar").click({force:true})
    cy.wait(6000)

    cy.get('body').contains("presencial").click({force:true})
    cy.wait(6000)

    const day = String(new Date().getDate())
    cy.get('body').contains(day).click({force:true})
    cy.wait(6000)

    const hour = String (new Date().getHours())
    cy.get('.ant-tabs-tabpane.ant-tabs-tabpane-active').contains(hour).first().click({force:true})
    cy.wait(6000)

    cy.get('body').contains("Confirmar reserva").click({force:true})
    cy.wait(6000)

    cy.get('body').contains("Descargar reservación").click({force:true})


    

})
})
