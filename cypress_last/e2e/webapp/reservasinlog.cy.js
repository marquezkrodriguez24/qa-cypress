describe('Ticket sin log', () => {
    beforeEach(() => {
      // Cypress starts out with a blank slate for each test
      // so we must tell it to visit our website with the `cy.visit()` command.
      // Since we want to visit the same URL at the start of all our tests,
      // we include it in our beforeEach function so that it runs before each test
  
    })
  
    it('ticket sin log', () => {


      cy.visit('https://www.zeroq.cl/offices/zeroq-qa-fisico')
      cy.wait(3000)
      //busca Botóndemo
   
      cy.get('body').contains("Admisión").click({force:true})

      cy.get('body').contains("Reservar").click({force:true})
      cy.wait(3000)

      const day = String(new Date().getDate())
      cy.get('body').contains(day).click({force:true})
      cy.wait(6000)
  
      const hour = String (new Date().getHours())
      cy.get('body').contains(hour).first().click({force:true})
      cy.wait(3000)

      cy.get('body').contains("Confirmar reserva").click({force:true})

      cy.get('body').contains("Continuar sin registrarte").click({force:true})
      cy.wait(6000)

          //tipear datos en reserva
          cy.get('input[name="rut"]').type("23584883-K")
          cy.wait(4000)
          cy.get('input[name="name"]').type("testing")
          cy.wait(4000)
          cy.get('input[name="email"]').type("test@zeroq.cl")
          cy.wait(4000)
          cy.get('input[name="phone"]').type("955662233")    
          cy.wait(4000)

        cy.get('button').contains("Reservar").click({force:true})
        cy.wait(30000)

        cy.get('body').contains("Entendido").click({force:true})
        cy.wait(4000)

          // verificar reserva creada
        cy.get('body').contains("Descargar")


  
    })
})