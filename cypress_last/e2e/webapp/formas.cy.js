describe('oficina con forma', () => {
    beforeEach(() => {
      // Cypress starts out with a blank slate for each test
      // so we must tell it to visit our website with the `cy.visit()` command.
      // Since we want to visit the same URL at the start of all our tests,
      // we include it in our beforeEach function so that it runs before each test
  
    })
  
    it('reserva con forma y sin log', () => {


      cy.visit('https://zeroq.cl/offices/demo-web-frontend')
      cy.wait(3000)
      //busca Botóndemo
   
      cy.get('body').contains("Evelyn").click({force:true})

      cy.get('body').contains("Reservar").click({force:true})
      cy.wait(3000)

      const day = String(new Date().getDate())
      cy.get('body').contains(day).click({force:true})
      cy.wait(6000)
  
      const hour = String (new Date().getHours())
      cy.get('body').contains(hour).first().click({force:true})
      cy.wait(3000)

      cy.get('body').contains("Confirmar reserva").click({force:true})

          //tipear datos en reserva
          cy.get('input[name="Nombre"]').type("testing")
          cy.wait(4000)
          cy.get('input[name="Correo"]').type("test@zeroq.cl")
          cy.wait(4000)
          cy.get('input[name="Codigo Cliente"]').type("9556")    
          cy.wait(4000)

          cy.get('.input__select-forms').select("option 1");

          cy.get('input[name="Dirección"]').type("Alameda 1112")

        cy.get('button').contains("reserva").click({force:true})
        cy.wait(30000)

        cy.get('body').contains("Continuar sin registrarte").click({force:true})
        cy.wait(6000)

        cy.get('body').contains("Entendido").click({force:true})
        cy.wait(4000)

          // verificar reserva creada
        cy.get('body').contains("Descargar")


  
    })

    it('con login ok', () => {

    cy.visit('https://zeroq.cl')
    cy.wait(5000)
    // LE doy click al Botón iniciar sesión
    cy.get('body').contains("Iniciar sesión").click({force: true})

    //tipear email
    cy.get('input[name="email"]').type("agente@zeroq.cl")
    cy.get('input[name="password"]').type("Z3r0Q-Q4-2021*/")

    cy.get('body').contains("Iniciar Sesión").click({force: true})
    cy.wait(6000)

    cy.visit('https://zeroq.cl/offices/demo-web-frontend')
    cy.wait(3000)
    //busca Botóndemo
 
    cy.get('body').contains("Evelyn").click({force:true})

    cy.get('body').contains("Reservar").click({force:true})
    cy.wait(3000)

    const day = String(new Date().getDate())
    cy.get('body').contains(day).click({force:true})
    cy.wait(6000)

    const hour = String (new Date().getHours())
    cy.get('body').contains(hour).first().click({force:true})
    cy.wait(3000)

    cy.get('body').contains("Confirmar reserva").click({force:true})

        //tipear datos en reserva
        cy.get('input[name="Nombre"]').clear().type("testing")
        cy.wait(4000)
        cy.get('input[name="Correo"]').clear().type("test@zeroq.cl")
        cy.wait(4000)
        cy.get('input[name="Codigo Cliente"]').clear().type("9556")    
        cy.wait(4000)

        cy.get('.input__select-forms').select("option 1");

        cy.get('input[name="Dirección"]').clear().type("Alameda 1112")

      cy.get('button').contains("reserva").click({force:true})
     
            // verificar reserva creada
      cy.get('body').contains("Descargar")


})
})