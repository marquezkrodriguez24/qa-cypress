function randomMs() { // min and max included 
    const min = 5
    const max = 20
    const random = Math.floor(Math.random() * (max - min + 1) + min)
    return random * 100
}
var genArr = Array.from({length: Cypress.env('loop')},(v,k)=> randomMs())

  
describe('Call End Default', () => {
    

    it('Succesfully performs login action', () => {
      cy.loginModulo()
      cy.get('.footer > :nth-child(1)').click()
    });
  
    
    it('recursive call end', () => {
        
        cy.wrap(genArr).each((index) => {
           
            // skip
            cy.wait(index)
            cy.get('.end').click()
        
        })
    });
  
  
  });
  