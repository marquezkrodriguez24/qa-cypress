// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

const resizeObserverLoopErrRe = /^[^(ResizeObserver loop limit exceeded)]/;
const pageLoginModule = require("../pages/Modules/login");
const pageLoginLocalWeb = require("../pages/LocalWeb/login");

Cypress.Commands.add("loginModule", (user) => {
  const { email, password } = user;
  pageLoginModule.handleLoginModule(email, password);
});

Cypress.Commands.add("loginLocalWeb", (user) => {
  const { email, password } = user;
  pageLoginLocalWeb.handleLoginLocalWeb(email, password);
});

Cypress.Commands.add("logoutModule", () => {
  pageLoginModule.handleLogoutModule();
});

Cypress.on("uncaught:exception", (err) => {
  /* returning false here prevents Cypress from failing the test */
  if (resizeObserverLoopErrRe.test(err.message)) {
    return false;
  }
});
