class pagePanel {
  elements = {
    btnRows: () => cy.get(".ant-layout-sider-children ul li").eq(1),
    itemsTableRow: (positionItem, positionSpan) =>
      cy
        .get(".ant-table-content .ant-table-row.ant-table-row-level-0")
        .eq(positionItem)
        .find("td span")
        .eq(positionSpan),
    inputReason: (text) =>
      cy.get('input[placeholder="Motivos de Atención"]').type(text),

    actionsTableRow: (positionItem, positionSpan, positionBtn) =>
      this.elements
        .itemsTableRow(positionItem, positionSpan)
        .find('button[type="button"]')
        .eq(positionBtn),
    btnAddReasons: () => cy.get(".ant-btn.ant-btn-primary.attention-button"),
    btnSaveChage: () => cy.get('button[type="submit"]'),
    btnCancel: () => cy.get(".ant-btn.ant-btn-reset"),

    listReasons: () =>
      cy.get(".ant-btn.ant-btn-primary.attention-button ~ div"),
    btnDeleteReason: () =>
      cy
        .get(
          ".ant-popover-content .ant-popover-inner-content .ant-popover-buttons button"
        )
        .eq(1),
  };

  //Item Filas
  handleAddReason = (reason) => {
    cy.log(3000);
    this.elements.inputReason(reason);
    this.elements.btnAddReasons().click();
    this.elements.btnSaveChage().click();
    this.elements.btnCancel().click();
  };

  handleDeleteReason = (indexIcon) => {
    this.elements.listReasons().find("> button").eq(indexIcon).click();
    this.elements.btnDeleteReason().click();
    this.elements.btnSaveChage().click();
    cy.wait(3000);
  };
}

module.exports = new pagePanel();
