const pageLoginModule = require("./login");
// const { getTickets } = require("../../services/ticket");
const { getNumberRandom } = require("../../utils/utilities");
class pageModule {
  elements = {
    btnArrive: (className) => cy.get(`${className} button[type="button"]`),
    btnVideoCall: () =>
      cy.get('.attention__container--button-wrapper button[type="videoAgain"]'),
    btnRecall: () => cy.get('button[type="recall"]'),
    btnMoreData: () => cy.get('div[type="open"] button'),
    btnCloseMoreData: () => cy.get('div[type="close"] button'),
    btnFinishAttention: () => cy.get('button[type="finish-attention"]'),
    btnSkip: () => cy.get('button[type="skip"]'),
    btnCallSkipped: () => cy.get('button[type="call-skipped"]'),
    btnDerive: () => cy.get('button[type="derive"]'),
    btnPauseModule: () =>
      cy.get(
        'div[style="width: 180px; position: relative; z-index: 3000;"] button[type="pause-module"]'
      ),
    btnModalPrimary: () => cy.get(".ant-modal-footer .ant-btn-primary"),
    btnModalDerivate: () => cy.get('button[type="derive-action"]'),
    btnManualCall: () => cy.get(".ticket__card-call-ticket-hover"),
    btnManualCallCenter: () =>
      cy.get(".ticket__card-information-container .ticket__card-call-ticket"),
    inputRUT: () =>
      cy.get('.ant-modal-content .ant-modal-body > div input[maxlength="10"]'),
    btnContinue: () =>
      cy.get(".ant-modal-content .ant-modal-body > div .ant-btn-primary"),
    btnCloseModalSuper: () => cy.get(".anticon.anticon-close"),
    btnSkipModalSuper: () => cy.get(".ant-modal-body button"),
    textFila: (position) =>
      cy.get(".attention__container-content-data strong").eq(position),
  };

  modals = Cypress.env("times").modals;
  recall = Cypress.env("times").recall;
  callTicket = Cypress.env("times").callTicket;
  handleIgnoreFirstIt() {
    return (
      Cypress.mocha.getRunner().suite.ctx.currentTest.title !==
      "Cargando opciones..."
    );
  }

  /**Funcion iniciar sesion handleLoginCompleted()
   *
   * isNextStep si el valor es:
   * Si el valor es igual a 0 es para procesar ticket.
   * Si el valor es igual a 1 no tiene que pasar del step (Oficina).
   * Si el valor es igual a 2 es para procesar ticket cuando se conecta/desconecta.
   *
   * isLogin si el valor es:
   * FALSE: hay que iniciar sesion
   * TRUE: no es necesario iniciar por procesos alternos
   *
   * office: Objeto que me indica la busqueda a realizar por input o por API
   *
   */

  async handleLoginCompleted(
    optionsLogin,
    office = Cypress.env("offices"),
    user
  ) {
    const { isNextStep, isLogin } = optionsLogin;
    const { typeModule } = office;

    if (!isLogin) cy.loginModule(user);
    cy.wait(2000);

    //Si es igual a uno isNextStep solo realizara el login, no buscara oficinas y modulos
    if (isNextStep === 1) return;

    //Buscamos la oficina
    pageLoginModule.searchOfficeOrModule(1, "S", office);

    /*Si no es superModulo y isNextStep igual a 0 realizamos el siguiente paso*/
    if (!typeModule && isNextStep !== 1) {
      pageLoginModule.handleClickBtnNext();
    }

    //Si cumple esta condicion es para ingresar al mismo modulo muchas veces conecta/desconecta
    if (isNextStep === 2) {
      pageLoginModule.searchOfficeOrModule(2, "S", office);
      pageLoginModule.handleClickBtnNext();
    }
  }

  /************************Funciones de modales**************************** */

  handleModalWarning() {
    cy.wait(this.modals);
    cy.get(".ant-modal-header .ant-modal-title").should(
      "have.contain",
      "Todavía estas atendiendo al"
    );

    cy.get(".ant-modal-body span").should(
      "have.contain",
      "Termina la atención antes de realizar esta acción."
    );

    this.elements.btnModalPrimary().within(() => {
      cy.contains("Terminar Atención").click({ force: true });
    });
  }

  handleModalFinishAttention(attention_reasons) {
    const { amount_reason, reason_v2 } = attention_reasons;
    cy.wait(amount_reason > 0 ? this.modals : 0);

    if (amount_reason > 0) {
      if (!reason_v2) {
        cy.get(".ant-checkbox-group-item")
          .eq(Math.floor(Math.random() * amount_reason))
          .click({ force: true });
      } else {
        const classModalReasonV2 =
          ".ant-modal-body .ant-collapse .ant-collapse-item";
        cy.get(`${classModalReasonV2} .ant-collapse-header .ant-radio`).each(
          (_, idxFolder, $folders) => {
            if (idxFolder === 0) {
              $folders[getNumberRandom($folders.length)].click({
                force: true,
              });

              cy.get(`${classModalReasonV2} .ant-checkbox-group-item`).each(
                (_, idxReason, $reasons) => {
                  if (idxReason === 0) {
                    $reasons[getNumberRandom($reasons.length)].click({
                      force: true,
                    });
                  }
                }
              );
            }
          }
        );
      }
      this.elements.btnModalPrimary().within(() => {
        cy.contains("Guardar").click({ force: true });
      });
    }
  }

  handleModalCallSkip() {
    cy.wait(this.modals);
    cy.get(".skiped-attention__title h4").should(
      "have.text",
      "Números Saltados"
    );
    cy.get(".skipped-attention_sub-title")
      .should(
        "have.contain",
        "Seleccione el ticket o reserva que desee volver a llamar"
      )
      .and(
        "have.contain",
        "Se visualizarán únicamente los números que tengas permitidos"
      );

    cy.get(".ant-table-content")
      .find('button[type="call-skipped"]')
      .eq(0)
      .click({ force: true });
  }

  handleModalDerive(optAttentions, slow) {
    const {
      filas,
      modules,
      isMassive,
      IsAutomaticOrManual,

      office: { typeModule },
    } = optAttentions;
    cy.wait(this.modals);
    let filasFilter = [];

    cy.get(".ant-modal-body .anticon.anticon-arrow-right  ~ h2").should(
      "have.text",
      "Derivación de atenciones"
    );

    if (!typeModule) {
      cy.get(".ant-modal-body").should(
        "contain.text",
        "Seleccione la fila o el ejecutivo al que desea derivar esta atención:"
      );
    }

    this.elements
      .btnModalDerivate()
      .should("contain.text", "Derivar Atención")
      .and("have.css", "background-color", "rgb(217, 217, 217)");

    cy.get(
      '.ant-tabs-content div[style="background-color: rgb(250, 250, 250);"] > span'
    ).each(($el, index, $list) => {
      const nameFila = $el.text();
      //Si no es masiva y es super modulo se le asigna las filas del modulo conectado
      //Si es masiva y no es super modulo se le asigna las filas de otros modulos que no sea donde esta conectado
      const newFilas =
        !isMassive || typeModule ? filas : modules[0].filasModule;

      let isExiste = newFilas.filter(({ name }) => name === nameFila).length;

      /*
        Si no es super modulo se deriva a la fila encontrada tando masiva o no masiva.
        Si no es masiva a su misma filas, y si es masiva a las filas de otro modulo
      */
      if (!typeModule) if (isExiste > 0) filasFilter.push(index);
      if (typeModule) if (isExiste === 0) filasFilter.push(index);

      if (index + 1 === $list.length) {
        let randomNumber = Math.floor(Math.random() * filasFilter.length);
        $list[filasFilter[randomNumber]].click({
          force: true,
        });
      }
    });

    this.elements
      .btnModalDerivate()
      .should("have.css", "background-color", "rgb(57, 16, 133)");

    if (slow) {
      cy.get(".derivated-card-title h3").should(
        "have.text",
        "¿Deseas pausar luego de derivar?"
      );

      cy.get(".derivated-card-content-buttons button")
        .first()
        .click({ force: true });

      this.elements.btnModalDerivate().click({ force: true });

      this.handleModalPause();
      this.elements.btnPauseModule().click({ force: true });
      cy.wait(3000);
      this.elements.btnPauseModule().click({ force: true });
    } else {
      this.elements.btnModalDerivate().click({ force: true });
    }

    if (!isMassive && IsAutomaticOrManual) cy.wait(this.callTicket);
  }

  handleModalPause() {
    cy.wait(this.modals);
    cy.get(".content__wrapper-pause .ant-typography").should(
      "have.text",
      "¿Por qué motivo deseas pausar?"
    );

    cy.get(".content__wrapper-pause button").eq(2).click({ force: true });
  }

  handleModalTypeClient(office) {
    const { typeModule } = office;
    this.handleArrived(typeModule);

    cy.get(".ant-modal-content .ant-modal-title").should(
      "have.text",
      "Tipo de cliente"
    );

    cy.get(".ant-modal-body .attention__check-title").should(
      "have.text",
      "Selecciona una opción"
    );

    cy.get(".ant-modal-body .ant-radio-wrapper").each((_, idxCheck, $list) => {
      if (idxCheck === 0) {
        let numberRamdon = getNumberRandom($list.length);
        const nameTypeClient = $list[numberRamdon].textContent;
        $list[numberRamdon].click({ force: true });

        this.elements.btnModalPrimary().within(() => {
          cy.contains("Guardar").click({ force: true });
        });

        //De momento estara QUEMADO
        if (nameTypeClient === "Mandatario" || nameTypeClient === "Asesor") {
          this.elements.inputRUT().type("9999999999");
          this.elements.btnContinue().click({ force: true });
        }
      }
    });
  }
  /************************Funciones de comandos**************************** */
  handleRecall() {
    for (let i = 0; i < 2; i++) {
      this.elements.btnRecall().click({ force: true });
      cy.wait(this.recall);
    }
  }

  handleArrived(typeModule) {
    this.elements
      .btnArrive(
        !typeModule ? ".data-client__container" : ".ant-col .ant-col-6 "
      )
      .click({ force: true });
  }

  handleVideoCall() {
    this.elements.btnVideoCall().click({ force: true });
    cy.wait(this.callTicket);
  }

  handleMoreData(questionsForm) {
    this.elements.btnMoreData().click({ force: true });

    questionsForm.forEach(({ question, answer }, idx) => {
      cy.get(".question-title").eq(idx).should("contain.text", question);
      cy.get(".answer-text").eq(idx).should("contain.text", answer);
    });

    this.elements.btnCloseMoreData().click({ force: true });

    // cy.get(".question-title").each((_, idxQuestion, $list) => {
    //   cy.get(".question-title").eq;
    // });
  }

  handleManualCallsCenter() {
    this.elements.btnManualCallCenter().click({ force: true });
    cy.wait(this.callTicket);
  }

  handleAttention(optAttentions) {
    const {
      filas,
      office: { typeModule },
    } = optAttentions;
    this.elements
      .textFila(!typeModule ? 0 : 1)
      .invoke("text")
      .then((textValue) => {
        let findFila = filas.filter(({ name }) => name === textValue)[0];

        const { attention_reasons } = findFila;

        this.elements.btnFinishAttention().eq(0).click({ force: true });

        this.handleModalFinishAttention(attention_reasons, filas);
      });
  }

  handleSkip(optAttentions) {
    const {
      office: { typeModule },
    } = optAttentions;
    this.elements.btnSkip().click({ force: true });
    if (typeModule)
      this.elements.btnSkipModalSuper().eq(1).click({ force: true });
    cy.wait(2000);
  }

  handleCallSkipped() {
    this.elements.btnCallSkipped().click({ force: true });
    this.handleModalCallSkip();
    cy.wait(this.callTicket);
  }

  handlePauseAttention(optAttentions) {
    const {
      filas,
      office: { typeModule },
    } = optAttentions;
    this.elements
      .textFila(!typeModule ? 0 : 1)
      .first()
      .invoke("text")
      .then((textValue) => {
        let findFila = filas.filter(({ name }) => name === textValue)[0];

        const { attention_reasons } = findFila;

        cy.wait(this.modals);
        this.elements.btnPauseModule().click({ force: true });
        this.handleModalPause();
        this.handleModalWarning();
        this.handleModalFinishAttention(attention_reasons, filas);
        cy.wait(8000);
        this.elements.btnPauseModule().click({ force: true });
      });
  }

  handleManualCalls(optAttentions) {
    const {
      filas,
      office: { typeModule },
    } = optAttentions;
    this.elements
      .textFila(!typeModule ? 0 : 1)
      .first()
      .invoke("text")
      .then((textValue) => {
        let findFila = filas.filter(({ name }) => name === textValue)[0];

        const { attention_reasons } = findFila;

        this.elements.btnManualCall().eq(0).click({ force: true });
        this.handleModalWarning();
        this.handleModalFinishAttention(attention_reasons, filas);
      });
  }

  /**
   * handleDerive()
   * filas: de un modulo,
   * slow: FALSE solo deriva, TRUE Deriva y Pausa
   * isMassive: FALSE 1 por 1, TRUE masiva
   */
  handleDerive(optAttentions, slow = false) {
    const {
      filas,
      office: { typeModule },
    } = optAttentions;

    this.elements
      .textFila(!typeModule ? 0 : 1)
      .first()
      .invoke("text")
      .then((textValue) => {
        let findFila = filas.filter(({ name }) => name === textValue)[0];

        const { attention_reasons } = findFila;

        this.elements.btnDerive().click({ force: true });

        if (!typeModule)
          this.handleModalFinishAttention(attention_reasons, filas);

        this.handleModalDerive(optAttentions, slow);
        // cy.wait(8000);
      });
  }

  handleValidateNotTickets(data, isDerivate) {
    const {
      office: { typeModule },
    } = data;
    if (!typeModule) {
      cy.get(".ant-collapse-content-box").should("not.exist");

      cy.get(".ant-row")
        .eq(1)
        .should("contain.text", "No hay atenciones en espera por el momento");

      this.elements
        .btnRecall()
        .should("contain.text", "Rellamar")
        .and("have.css", "background-color", "rgb(217, 217, 217)");

      this.elements
        .btnFinishAttention()
        .eq(0)
        .should("contain.text", "Terminar Atención")
        .and("have.css", "background-color", "rgb(217, 217, 217)");

      this.elements
        .btnSkip()
        .should("contain.text", "Saltar Atención")
        .and("have.css", "background-color", "rgb(217, 217, 217)");

      this.elements
        .btnDerive()
        .should("contain.text", "Derivar Número")
        .and("have.css", "background-color", "rgb(217, 217, 217)");
    }

    if (isDerivate) this.handleDisconnect();
  }

  handleMassDisconnect(optAttentions) {
    const {
      filas,
      office,
      isMassive,
      office: { typeModule },
    } = optAttentions;
    this.elements
      .textFila(!typeModule ? 0 : 1)
      .first()
      .invoke("text")
      .then((textValue) => {
        let findFila = filas.filter(({ name }) => name === textValue)[0];

        const { attention_reasons } = findFila;

        this.handleDisconnect();
        this.handleModalWarning();
        this.handleModalFinishAttention(attention_reasons, filas);

        const optionsLogin = {
          isNextStep: 2,
          isLogin: true,
        };

        if (isMassive) this.handleLoginCompleted(optionsLogin, office);
      });
  }

  handleDisconnect() {
    cy.wait(2500);
    cy.get("#root header button").eq(1).click({ force: true });
  }

  /*********************Funciones logicas***********************************************/

  handleFirstCall(data, commandFirstCall) {
    console.log("commandFirstCall", commandFirstCall);
    const { office, questionsForm } = data;
    if (!commandFirstCall) return;
    /**
     * M: Botón Manual
     * A: Botón llego
     * V: Botón Video llamada
     * O: Botón más datos
     */

    commandFirstCall.split("").forEach((letter) => {
      if (letter === "M") {
        this.handleManualCallsCenter();
      } else if (letter === "A") {
        this.handleModalTypeClient(office);
      } else if (letter === "C") {
        this.elements.btnCloseModalSuper().click({ force: true });
      } else if (letter === "O") {
        this.handleMoreData(questionsForm);
      }
    });
  }

  /**
   *
   * @param {*} position equivalente al ticket que se esta atiendiendo
   *
   * @param {*} data Informacion general del proceso (Filas,ticket,modulos, oficina, etc)
   *
   * @param {*} command Comando el cual el ticket se tiene que atender
   */
  handleStructCombinations(position, data, command) {
    let finish = false;
    // const commandText = [
    //   { type: "T", name: "Terminar Atención" },
    //   { type: "R", name: "Rellamar" },
    //   { type: "S", name: "Saltar Atención" },
    //   { type: "L", name: "Llamar Saltado    " },
    //   { type: "D", name: "Derivar Número" },
    //   { type: "N", name: "Derivar Número-Pausar" },
    //   { type: "P", name: "Pausar Módulo" },
    //   { type: "C", name: "Desconectarse" },
    //   { type: "F", name: "LLamado Futuro" },
    //   { type: "V", name: "Conectarse / Desconectarse" },
    // ];

    const {
      office,
      modules,
      filas,
      numberTickets,
      options,
      IsAutomaticOrManual,
      questionsForm,
    } = data;
    const { isMassive } = options;

    let optAttentions = {
      office,
      filas,
      modules,
      isMassive,
      IsAutomaticOrManual,
    };

    command.split("").forEach((letter) => {
      switch (letter) {
        case "A":
          this.handleModalTypeClient(office);
          break;
        case "M":
          this.handleManualCallsCenter();
          break;
        case "O":
          this.handleMoreData(questionsForm);
          break;
        case "T":
          this.handleAttention(optAttentions);
          break;
        case "R":
          this.handleRecall();
          break;
        case "S":
          this.handleSkip(optAttentions);
          break;
        case "L":
          this.handleCallSkipped();
          //Si es llamado saltado no se realiza el primer llamada si no hasta el final
          break;
        case "D":
          this.handleDerive(optAttentions, false);
          break;
        case "N":
          optAttentions.isMassive = false;
          this.handleDerive(optAttentions, true);
          break;
        case "P":
          this.handlePauseAttention(optAttentions);
          break;
        case "C":
          this.handleMassDisconnect(optAttentions);
          break;

        case "F":
          if (numberTickets < position && numberTickets > 1) {
            this.handleManualCalls(optAttentions);
          } else {
            finish = true;
            this.handleAttention(optAttentions);
          }
          break;

        case "V":
          this.handleMassDisconnect(optAttentions);
          finish =
            numberTickets === 1 && numberTickets === position + 1
              ? false
              : true;
          break;
        case "U":
          this.handleVideoCall();
          break;
        default:
          break;
      }
    });

    return finish;
  }

  /**
   * @param {*} data Informacion general del proceso (Filas,ticket,modulos, oficina, etc)
   *
   * @param {*} optCommands Opciones de como se tiene que atender un ticket
   */

  handleComponentAttentions(data, optCommands) {
    let finish = false;
    const {
      office,
      numberTickets,
      IsAutomaticOrManual,
      options: { isGetTicketSkip },
    } = data;
    let { typeFirstCall, command, isDerivate } = optCommands;
    const { typeModule } = office;

    if (numberTickets === 0) finish = true;
    if (!typeModule) {
      //Buscamos el modulo
      pageLoginModule.searchOfficeOrModule(2, "S", office);
    }
    pageLoginModule.handleClickBtnNext();

    let countTicketRefresh = numberTickets;

    for (let idx = 0; idx < countTicketRefresh; idx++) {
      if (IsAutomaticOrManual && !isGetTicketSkip) cy.wait(this.callTicket);
      //A futuro eliminar handleFirstCall de momento se dejara
      this.handleFirstCall(data, typeFirstCall);
      finish = this.handleStructCombinations(idx, data, command);

      //NOTA: tratar de realizar el refresh
      /* const { officeId, moduleId } = office;
      cy.wrap(
        getTickets(
          `/turn-o-matic/offices/${officeId}/modulos/${moduleId}/tickets`,
          typeModule
        )
      ).then((data) => {
        console.log("countTicketRefresh", countTicketRefresh);
        console.log(data);
      });*/

      if (numberTickets === idx + 1) finish = true;
    }

    if (finish) this.handleValidateNotTickets(data, isDerivate);
  }
}

module.exports = new pageModule();
