class pageLoginModule {
  elements = {
    userEmail: () => cy.get('input[type="email"]'),
    passwordInput: () => cy.get('input[type="password-default"]'),
    btnPrimary: () => cy.get('button[type="primary"]'),
    btnBack: () => cy.get('button[type="backgroundLessGray"]'),
    steps: () => cy.get(".ant-steps .ant-steps-item"),
  };

  typeEmail(username) {
    this.elements.userEmail().type(username);
  }

  typePassword(password) {
    this.elements.passwordInput().type(password);
  }

  handleClickBtnNext() {
    this.elements.btnPrimary().click();
  }

  handleLoginModule(username, password) {
    this.typeEmail(username);
    this.typePassword(password);
    this.handleClickBtnNext();
  }

  /**
   *
   * @param {*} type
   *  1 = equivale a oficina / 2 = equivale a modul
   *
   * @param {*} action
   * S = equivalente busqueda por el input / C = equivalente a darle click a la lista
   *
   * @param {*} office oficina la cual esta conectada
   */

  searchOfficeOrModule(type, action, office) {
    const { id, nameOffice, nameModule, moduleConected, typeModule } = office;

    let name = type === 1 ? nameOffice : nameModule;

    const msj =
      type === 1 ? `Oficina ${name} seleccionada` : `${name} seleccionado`;

    if (action === "S") {
      cy.get(".ant-input").type(name);
    } else if (action === "C") {
      if (type === 1) {
        cy.get(`#${id}`).click();
      } else {
        cy.get(`#${moduleConected.moduleId}`).click();
      }
    }

    if (type === 1 && typeModule) {
      cy.get(".ant-radio-group .ant-radio-input").eq(1).click();
    } else {
      cy.get(".ant-modal-body").should("contain.text", msj);
    }
  }

  handleValidateNextStep(stepMenu) {
    /**
     * stepMenu si es igual a
     * 0 : cierre sesion
     * 1 : entro al step oficina
     * 2 : entro al step module
     */

    cy.wait(3000);

    const classNameArray = [
      "ant-steps-item-active",
      "ant-steps-item-wait",
      "ant-steps-item-finish",
    ];

    this.elements.steps().each(($step, index) => {
      let position = 0;
      if (stepMenu === 0) {
        position = index === 0 ? 0 : 1;
      } else if (stepMenu === 1) {
        cy.get(".ant-modal-body h2").should("have.text", "Módulo funcionario");
        cy.get(".ant-modal-body h3").should(
          "have.text",
          "Selecciona una opción para continuar"
        );
        position = index === 0 ? 2 : index === 1 ? 0 : 1;
      } else if (stepMenu === 2) {
        cy.get(".ant-col h5").should("have.text", "Oficina seleccionada");
        position = index === 2 ? 0 : 2;
      }

      expect($step).to.have.class(classNameArray[position]);
    });
  }

  handleLogoutModule() {
    cy.get("#root div>.ant-dropdown-trigger").should(($item) => {
      expect($item[1]).to.contain.text(Cypress.env("users")[0].email);
    });

    cy.get("#root div>.ant-dropdown-trigger").eq(1).click();

    cy.get(".ant-dropdown").should("not.have.class", "ant-dropdown-hidden");

    cy.get(".ant-dropdown > ul > li").should("have.text", "Cerrar sesión");
    cy.get(".ant-dropdown > ul > li").click();

    //Validamos que se haya realizado el cierre de sesión
    this.handleValidateNextStep(0);
  }
}

module.exports = new pageLoginModule();
