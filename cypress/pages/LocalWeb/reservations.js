class pageReservations {
  elements = {
    btnReservation: () => cy.get('div[type="reservations"]'),
    btnRows: (props) => cy.get(`div[title="${props}"]`),
    daysCalendar: () =>
      cy.get(
        ".react-calendar__tile.react-calendar__month-view__days__day:not([disabled])"
      ),
    tabsTime: () => cy.get(".ant-tabs-nav-list .ant-tabs-tab"),
    blocksTime: () =>
      cy.get(
        ".ant-tabs-content-holder .ant-tabs-content-top .ant-tabs-tabpane-active div"
      ),
    breadCrumb: () => cy.get(".ant-breadcrumb ol li"),
    stepActionFinal: () => cy.get(".steps-action button"),
  };

  handleReservation() {
    this.elements.btnReservation().click();
  }
}

module.exports = new pageReservations();
