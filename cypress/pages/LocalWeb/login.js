class pageLoginLocalWeb {
  elements = {
    userEmail: () => cy.get('input[name="email"]'),
    passwordInput: () => cy.get('input[name="password"]'),
    btnLogin: () => cy.get('div[stroke="#fff"]'),
    headerMenu: () => cy.get('button[action="openDrawer"]'),
    btnLogout: () => cy.get('button[action="logout"]'),
  };

  typeEmail(username) {
    this.elements.userEmail().type(username);
  }

  typePassword(password) {
    this.elements.passwordInput().type(password);
  }

  handleClickBtnLogin() {
    this.elements.btnLogin().click();
  }

  handleLoginLocalWeb(username, password) {
    this.typeEmail(username);
    this.typePassword(password);
    this.handleClickBtnLogin();
  }

  handleLogout() {
    cy.wait(3000);
    this.elements.headerMenu().click({ force: true });
    cy.wait(3000)
    this.elements.btnLogout().click();
  }
}

module.exports = new pageLoginLocalWeb();
