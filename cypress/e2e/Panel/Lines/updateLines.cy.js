import pageLoginModule from "../../../pages/Modules/login";
import pageModule from "../../../pages/Modules/module";

import pagePanel from "../../../pages/Panel/panel";

import {
  getBodyAttetionTicket,
  getOfficeStorage,
  getModulesOrFilas,
  getOffice,
} from "../../../services/ticket";

describe("Actualizar filas", () => {
  beforeEach(() => {
    const optionsLogin = {
      isNextStep: 0,
      isLogin: false,
      
    };
    const user = Cypress.env("users")[0];
    const office = getOffice(user.offices, false);

    cy.wrap(Cypress.env("times").callTicket).as("timeOut");
    cy.wrap(office).as("office");
    cy.wrap({
      isMassive: true,
      isCreateTicket: true,
      isGetTicketSkip: false,
    }).as("options");

    cy.visit(Cypress.env("pageModule"));
    pageModule.handleLoginCompleted(optionsLogin, office, user);

    if (Cypress.env("stagingOrPrd") === "OFF") {
      //Local WEB
      cy.visit(`${Cypress.env("pageLocalWeb")}/signin`);
      cy.loginLocalWeb(user);
    }

    cy.wait(3000);

    cy.wrap("Reason ADD x2").as("reason");
  });

  it.skip("Consultando", function () {
    let finish = false;

    cy.wrap(getBodyAttetionTicket(this.office, this.options, false)).then(
      ({ tickets, filas, numberTickets }) => {
        if (numberTickets === 0) finish = true;
        pageLoginModule.handleClickBtnNext();

        tickets.forEach((_, idx) => {
          cy.wait(this.timeOut);
          pageModule.handleAttention(filas);
          if (numberTickets === idx + 1) finish = true;
        });

        if (finish) pageModule.handleValidateNotTickets();
      }
    );
  });

  it("Agregar motivo", function () {
    cy.wrap(getModulesOrFilas(this.office)).then(({ filas }) => {
      const { slug } = getOfficeStorage(this.office);
      cy.visit(`${Cypress.env("pageLocalWeb")}/control/#/office/${slug}`);
      cy.wait(3000);
      pagePanel.elements.btnRows().click();

      filas.forEach(({ name }, idx) => {
        if (idx > 0) return;
        cy.get(".ant-table-content .ant-table-row.ant-table-row-level-0").each(
          ($el, idxRow) => {
            pagePanel.elements.itemsTableRow(idxRow, 0).each(($el) => {
              const nameFila = $el.text();
              if (name === nameFila) {
                pagePanel.elements.actionsTableRow(idxRow, 5, 0).click();
                pagePanel.handleAddReason(this.reason);
              }
            });
          }
        );
      });
    });
  });

  it("Eliminando motivo", function () {
    cy.wrap(getModulesOrFilas(this.office)).then(({ filas }) => {
      const { slug } = getOfficeStorage(this.office);
      cy.visit(`${Cypress.env("pageLocalWeb")}/control/#/office/${slug}`);
      cy.wait(3000);
      pagePanel.elements.btnRows().click();

      filas.forEach(({ name }, idx) => {
        if (idx > 0) return;
        cy.get(".ant-table-content .ant-table-row.ant-table-row-level-0").each(
          (_, idxRow) => {
            pagePanel.elements.itemsTableRow(idxRow, 0).each(($el) => {
              const nameFila = $el.text();
              if (name === nameFila) {
                pagePanel.elements.actionsTableRow(idxRow, 5, 0).click();
                cy.wait(8000);
                pagePanel.elements.listReasons().each(($el, index) => {
                  console.log("nameee", $el.text());
                  if ($el.text() === this.reason) {
                    console.log("SIII");
                    pagePanel.handleDeleteReason(index);
                  }
                });
              }
            });
          }
        );
      });
    });
  });
});
