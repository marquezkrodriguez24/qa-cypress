import pageModule from "../../../pages/Modules/module";
import pageLoginLocalWeb from "../../../pages/LocalWeb/login";
import pageReservations from "../../../pages/LocalWeb/reservations";

import {
  getAllFilasWithBlocks,
  getOfficeStorage,
  getOffice,
} from "../../../services/ticket";

describe("Reservaciones - Bloques", () => {
  beforeEach(() => {
    const optionsLogin = {
      isNextStep: 1,
      isLogin: false,
      
    };

    const user = Cypress.env("users")[0];
    const office = getOffice(user.offices, false);
    cy.wrap(office).as("office");

    cy.visit(Cypress.env("pageModule"));
    pageModule.handleLoginCompleted(optionsLogin, office, user);

    //Si es desarrollo iniciamos sesion ya que el dominio cambia y
    //en produccion si entramos en el login del module, nos ahorramos el del local
    if (Cypress.env("stagingOrPrd") === "OFF") {
      cy.visit(`${Cypress.env("pageLocalWeb")}/signin`);
      cy.loginLocalWeb(user);
    }

    cy.wait(3000);
  });

  //Buscar un endpoint donde traiga la info de la empresa
  //Tratar de re-armar este TEST ordenadamente

  it("Validar bloques API", function () {
    const dateNow = new Date();
    const year = dateNow.getFullYear(),
      month = dateNow.getMonth() + 1;
    const {
      block: { countRows, countDays },
    } = Cypress.env("reservation");

    const { slug, timezone } = getOfficeStorage(this.office);

    cy.wrap(getAllFilasWithBlocks(slug)).then((filas) => {
      // console.log("filas", filas);
      Object.keys(filas).map((slugFile, idx) => {
        // console.log("slugFile", slugFile);
        if (countRows > idx) {
          cy.request(
            `${Cypress.env(
              "hostReserve"
            )}/reservations/api/v2/offices/${slug}/${slugFile}/${year}/${month}/timeblocks/counter?tz=${timezone}`
          ).should((response) => {
            const { body: dataDays, status } = response;
            expect(status).to.eq(200);

            const daysEnabled = dataDays.filter(
              ({ bookable }) => bookable === true
            );
            if (daysEnabled.length > 0) {
              daysEnabled.map(({ date }, idx) => {
                if (countDays > idx) {
                  cy.log(7000);
                  cy.request(
                    `${Cypress.env(
                      "hostReserve"
                    )}/reservations/api/v2/offices/${slug}/${slugFile}/${date}/timeblocks`
                  ).should((response) => {
                    const { body: dataBlock, status } = response;
                    const blockCountList = {};

                    expect(status).to.eq(200);

                    //Contamos los bloques, por si viene mas de 1
                    dataBlock
                      .map(({ from }) => from.slice(from.search("T") + 1, -8))
                      .forEach((item) => {
                        blockCountList[item] = !blockCountList[item]
                          ? 1
                          : (blockCountList[item] += 1);
                      });

                    //Recorremos los bloques contados para validar a nivel de assert que no
                    //vengan repetidos
                    for (let value of Object.values(blockCountList)) {
                      let msj =
                        value > 1 ? "Bloque Incorrecto" : "Bloque Correcto";
                      expect(msj).to.eq("Bloque Correcto");
                      expect(value).to.eq(1);
                    }
                  });
                }
              });
            }
          });
        }
      });
    });
  });

  it("Validar bloques WEB", function () {
    const { slug } = getOfficeStorage(this.office);
    cy.visit(`${Cypress.env("pageLocalWeb")}/offices/${slug}`);
    //Buscar la oficina
    // cy.get(".searchContainer input").type("Kevin QA");

    cy.wrap(getAllFilasWithBlocks(slug)).then((filas) => {
      // console.log("filas", filas);
      Object.keys(filas).map((row) => {
        const { name } = filas[row];
        //Seleccionar fila click
        pageReservations.elements.btnRows(name).click();

        //Botón reserva click
        pageReservations.elements.btnReservation().click();

        pageReservations.elements.daysCalendar().each((_, index, $calendar) => {
          if (index === 0) {
            //Click al dia del calendar
            $calendar[index].click();
            // cy.wait(3000); habilitar si da error

            pageReservations.elements.tabsTime().each((_, idxTabs, $tabs) => {
              let blockAnt = "";
              cy.wait(5000);
              //Buscamos los bloques
              pageReservations.elements
                .blocksTime()
                .each(($el, idxBlock, $listBlocks) => {
                  const blockAct = $el.text();
                  expect(blockAnt).to.not.equal(blockAct);
                  blockAnt = blockAct;
                  if (
                    idxBlock + 1 === $listBlocks.length &&
                    idxTabs + 1 < $tabs.length
                  ) {
                    $tabs[idxTabs + 1].click();
                  }
                });
            });
          }
        });
        //Terminamos de validar la fila, regresamos a seleccionar otra
        pageReservations.elements.breadCrumb().eq(2).click();
        // cy.wait(5000);
      });
    });
    pageLoginLocalWeb.handleLogout();
  });

  it("Realizar reservación bloques nocturnos", function () {
    const { slug } = getOfficeStorage(this.office);
    cy.visit(`${Cypress.env("pageLocalWeb")}/offices/${slug}`);

    cy.wrap(getAllFilasWithBlocks(slug)).then((filas) => {
      Object.keys(filas).map((row, _) => {
        const { name } = filas[row];
        //Seleccionar fila click
        pageReservations.elements.btnRows(name).click();

        //Botón reserva click
        pageReservations.elements.btnReservation().click();

        pageReservations.elements.daysCalendar().each((_, index, $calendar) => {
          if (index === 0) {
            $calendar[index].click();

            pageReservations.elements.tabsTime().each((_, idxTabs, $tabs) => {
              if ($tabs.length === idxTabs + 1) {
                pageReservations.elements
                  .blocksTime()
                  .each((_, idxBlock, $listBlocks) => {
                    if (idxBlock + 1 === $listBlocks.length) {
                      $listBlocks[idxBlock].click();
                      cy.get(".current-step-label").should(
                        "have.text",
                        " Último paso"
                      );
                      cy.get('div[style="width: 100%;"] h5').should(
                        "have.text",
                        "Completa los datos antes de confirmar"
                      );

                      pageReservations.elements.stepActionFinal().eq(1).click();

                      cy.get(".line__name").should("have.text", name);
                    }
                  });
              } else {
                $tabs[idxTabs + 1].click();
              }
            });
          }
        });
        //Terminamos de validar la fila, regresamos a seleccionar otra
        cy.visit(`${Cypress.env("pageLocalWeb")}/offices/${slug}`);
      });
    });
    pageLoginLocalWeb.handleLogout();
  });
});
