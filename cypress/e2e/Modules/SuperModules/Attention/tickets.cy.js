const pageModule = require("../../../../pages/Modules/module");
const {
  getBodyAttetionTicket,
  changeOptionsOffice,
  getOffice,
} = require("../../../../services/ticket");

describe("Súper Modulo - Atendiendo tickets Masivos", () => {
  beforeEach(function () {
    const optionsLogin = {
      isNextStep: 0,
      isLogin: false,
    };

    const user = Cypress.env("users")[0];
    const office = getOffice(user.offices, true);
    cy.wrap({
      isMassive: true,
      isCreateTicket: true,
      isGetTicketSkip: false,
    }).as("options");
    cy.wrap(Cypress.env("times").callTicket).as("timeOut");
    cy.wrap(office).as("office");

    if (pageModule.handleIgnoreFirstIt()) {
      cy.visit(Cypress.env("pageModule"));
      pageModule.handleLoginCompleted(optionsLogin, office, user);

      //ABRIR el kiosko
      const windowKisko = window.open(
        "https://zeroq.cl/kiosk?officeId=1777&kioskId=13169&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
        "_blank"
      );

      cy.wrap(windowKisko).as("windowKisko");
      cy.wait(10000);
    }
  });

  it("Cargando opciones...", function () {
    let optionsChange = {
      webServiceAutoCall: false,
      attention_arrived: false,
      client_type: false,
    };
    cy.wrap(changeOptionsOffice(this.office.id, optionsChange));
    cy.wait(2500);
  });

  it("Atendiendo tickets - Terminar Atención", function () {
    cy.wrap(getBodyAttetionTicket(this.office, this.options, true)).then(
      (data) => {
        const optionsAtt = {
          timeOut: this.timeOut * 2,
          command: "T",
          typeFirstCall: "M",
        };

        pageModule.handleComponentAttentions(data, optionsAtt);
      }
    );
  });

  it("Atendiendo tickets - Saltar Atención", function () {
    cy.wrap(getBodyAttetionTicket(this.office, this.options, true)).then(
      (data) => {
        const optionsAtt = {
          timeOut: this.timeOut * 2,
          command: "S",
          typeFirstCall: "MC",
        };

        pageModule.handleComponentAttentions(data, optionsAtt);
      }
    );
  });

  it("Atendiendo tickets - Derivar Número/Terminar Atención", function () {
    cy.wrap(getBodyAttetionTicket(this.office, this.options, true)).then(
      (data) => {
        const optionsAtt = {
          timeOut: this.timeOut * 2,
          command: "D",
          typeFirstCall: "MC",
          isDerivate: true,
        };

        pageModule.handleComponentAttentions(data, optionsAtt);
      }
    );
  });

  afterEach(function () {
    if (pageModule.handleIgnoreFirstIt()) this.windowKisko.close();
  });
});
