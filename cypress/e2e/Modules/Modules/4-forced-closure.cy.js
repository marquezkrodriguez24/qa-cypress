/*** Atención de tikcets masivos 1 a muchos ***/
// const pageLoginModule = require("../../pages/Modules/login");
const pageModule = require("../../../pages/Modules/module");
const {
  getBodyAttetionTicket,
  getElapse,
  getOffice,
  attendTicket,
} = require("../../../services/ticket");

describe("Modulos - Cierre forzoso", () => {
  beforeEach(() => {
    const optionsLogin = {
      isNextStep: 1,
      isLogin: false,
      
    };
    const user = Cypress.env("users")[0];
    const office = getOffice(user.offices, false);

    cy.wrap({
      isMassive: false,
      isCreateTicket: true,
      isGetTicketSkip: false,
    }).as("options");

    cy.wrap(Cypress.env("times").callTicket).as("timeOut");
    cy.wrap(office).as("office");

    cy.visit(Cypress.env("pageModule"));
    pageModule.handleLoginCompleted(optionsLogin, office, user);
  });

  it("Atención de un ticket - Obtener CIUD", function () {
    cy.wrap(getBodyAttetionTicket(this.office, this.options, false)).then(
      ({ tickets, office: { moduleConected } }) => {        
        tickets.forEach((ticket, idx) => {
          if (idx !== 0) return;
          //1. Atendemos el ticket para sacar el CUID
          cy.wrap(attendTicket(moduleConected, ticket)).then((result) => {
            const { cuid } = result;
            cy.log(cuid);
            cy.wrap(cuid).as("ciud");

            //2. Esperamos los 5 minutos
            cy.wait(60 * 5 * 1000);
          });
        });
      }
    );
  });

  it("Verificar el Elapse", function () {
    //3. Realizamos la peticion para saber si esta OK con el CUID
    cy.log("ciud", this.ciud);
    cy.wrap(getElapse(this.office.id)).then((item) => {
      let msj = !JSON.parse(item[this.ciud]).elapsed
        ? "Elapse no encontrado"
        : "Elapse encontrado";

      cy.log(JSON.parse(item[this.ciud]).elapsed);
      expect("Elapse encontrado").to.equal(msj);
    });
  });
});
