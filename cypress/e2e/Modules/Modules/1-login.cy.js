import pageLoginModule from "../../../pages/Modules/login";

describe("Inicio de sesión - Module", () => {
  beforeEach(() => {
    cy.visit(Cypress.env("pageModule"));
  });

  it("Inicio de sesión - CORRECTO", () => {
    cy.loginModule(Cypress.env("users")[0]);
    pageLoginModule.handleValidateNextStep(1);
  });

  it("Inicio de sesión - INCORRECTO", () => {
    cy.loginModule({ email: "hola@hola.com", password: "km199717*" });

    cy.intercept(
      "POST",
      "https://o992282.ingest.sentry.io/api/5949743/store/?sentry_key=c23bf91fc3b5457dafdac1a0f65edbf3&sentry_version=7"
    ).as("getComment");

    cy.wait("@getComment").its("response.statusCode").should("eq", 200);
  });

  it("Inicio/Cierre de sesión", () => {
    cy.loginModule(Cypress.env("users")[0]);
    pageLoginModule.handleValidateNextStep(1);
    cy.logoutModule();
  });
});
