const pageLoginModule = require("../../../pages/Modules/login");
const pageModule = require("../../../pages/Modules/module");
const { getOffice, getModulesOrFilas } = require("../../../services/ticket");

describe("Modulos", () => {
  beforeEach(() => {
    const user = Cypress.env("users")[0];
    const office = getOffice(user.offices, false);
    cy.wrap(office).as("office");
    cy.visit(Cypress.env("pageModule"));
    cy.loginModule(user);

    pageLoginModule.searchOfficeOrModule(1, "S", office);
    pageLoginModule.handleClickBtnNext();
  });

  it("Buscar Modulos", function () {
    cy.wrap(getModulesOrFilas(this.office)).then((data) => {
      console.log(data)
      pageLoginModule.searchOfficeOrModule(2, "S", this.office);
      pageLoginModule.handleClickBtnNext();
      //Validamos que este atendiendo ticket
      cy.get(".ant-row h4")
        .filter('[class="ant-typography"]')
        .should("have.text", "Atención en fila");

      pageModule.handleDisconnect();

      pageLoginModule.handleValidateNextStep(1);
    });
  });

  it("Seleccionar Modulos", function () {
    cy.wrap(getModulesOrFilas(this.office)).then((_) => {
      pageLoginModule.searchOfficeOrModule(2, "C", this.office);
      pageLoginModule.handleClickBtnNext();

      //Validamos que este atendiendo ticket
      cy.get(".ant-row h4")
        .filter('[class="ant-typography"]')
        .should("have.text", "Atención en fila");

      pageModule.handleDisconnect();

      pageLoginModule.handleValidateNextStep(1);
    });
  });
});
