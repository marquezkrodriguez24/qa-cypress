const pageLoginModule = require("../../../pages/Modules/login");
const { getOffice } = require("../../../services/ticket.js");

describe("Oficinas", () => {
  beforeEach(() => {
    const optionsLogin = {
      isNextStep: 0,
      isLogin: false,
      
    };

    const user = Cypress.env("users")[0];
    const office = getOffice(user.offices, false);
    cy.wrap(office).as("office");
    cy.visit(Cypress.env("pageModule"));
    cy.loginModule(user);
  });

  it("Buscar oficinas", function () {
    pageLoginModule.searchOfficeOrModule(1, "S", this.office, false);
    pageLoginModule.handleValidateNextStep(1);
  });

  it("Seleccionar oficinas", function () {
    pageLoginModule.searchOfficeOrModule(1, "C", this.office, false);
    pageLoginModule.handleValidateNextStep(1);
  });
});
