/*** Atención de tikcets masivos 1 a muchos ***/
const pageModule = require("../../../../../pages/Modules/module");
const {
  getBodyAttetionTicket,
  getOffice,
  changeOptionsOffice,
  connectOrDisConnectModule,
} = require("../../../../../services/ticket");

const { getTotAssemble } = require("../../../../../utils/utilities");

const testsAutomatic = require("../../../../../fixtures/Modules/type_attention.json");

describe("Modulos - Atendiendo tickets Masivos - Manual", function () {
  //Realizar el ciclo
  testsAutomatic.forEach((data) => {
    const IsAutomaticOrManual = false;
    const typeForm = true;
    let { skip, describeName, optionsChange, itsIteration, typeAction } = data;

    //Puede ser automatico o manual
    optionsChange = {
      ...optionsChange,
      webServiceAutoCall: IsAutomaticOrManual,
    };

    if (!skip) return;
    data.itsIteration = getTotAssemble([...itsIteration], {
      typeAction,
      IsAutomaticOrManual,
      typeForm,
    });

    let optionsLogin = {
      isNextStep: 0,
      isLogin: false,
    };
    const user = Cypress.env("users")[0];
    const office = getOffice(user.offices, false);

    describe(describeName, function () {
      before(() => {
        cy.wrap(0).as("enterOnce");
        cy.visit(Cypress.env("pageModule"), { timeout: 15000 });
        cy.wrap(changeOptionsOffice(office.id, optionsChange));
        cy.wait(2500);
        //aqui no tratar de que busque la oficina solo el logue al paso 2
        pageModule.handleLoginCompleted(optionsLogin, office, user);
      });

      beforeEach(function () {
        cy.wrap({
          isMassive: true,
          isCreateTicket: true,
          isGetTicketSkip: false,
          isVideoCall: typeAction === 2 ? true : false,
          typeForm,
        }).as("options");

        cy.wrap(office).as("office");
        if (this.enterOnce === 0) {
          cy.wrap(1).as("enterOnce");
          cy.wrap(localStorage.getItem("user_data")).as("user_data");
          cy.wrap(localStorage.getItem("web-service-offices")).as(
            "web_service_offices"
          );
        } else {
          console.log("*****OFICINA*****");
          optionsLogin = { ...optionsLogin, isLogin: true };
          cy.wrap(optionsLogin).as("optionsLogin");
          pageModule.handleLoginCompleted(optionsLogin, office, user);
          localStorage.setItem("user_data", this.user_data);
          localStorage.setItem("web-service-offices", this.web_service_offices);
        }
      });

      ///Mapear los IT
      itsIteration.map((itemIt, _) => {
        const {
          skip,
          nameCommand,
          typeFirstCall,
          command,
          isDerivate,
          isGetTicketSkip,
        } = itemIt;

        if (!skip) return;

        it(nameCommand, function () {
          cy.log(typeFirstCall + "" + command);

          cy.wrap(
            getBodyAttetionTicket(
              this.office,
              { ...this.options, isGetTicketSkip },
              false
            )
          ).then((data) => {
            pageModule.handleComponentAttentions(
              { ...data, IsAutomaticOrManual },
              itemIt
            );

            if (command === "D") {
              const { modules } = data;

              cy.wrap(modules).then((data) => {
                connectOrDisConnectModule(true, null, false);
                cy.go("back");
                this.options.isCreateTicket = false;
                data.forEach((module) => {
                  cy.wrap(module).then((itemMod) => {
                    optionsLogin.isLogin = true;
                    pageModule.handleLoginCompleted(optionsLogin, itemMod);
                    cy.wrap(getBodyAttetionTicket(itemMod, this.options)).then(
                      (data) => {
                        const optionsAtt = {
                          ...itsIteration[0],
                          isDerivate,
                        };
                        pageModule.handleComponentAttentions(
                          { ...data, IsAutomaticOrManual },
                          optionsAtt
                        );
                      }
                    );
                  });
                });
              });
            }
          });
        });
      });

      afterEach(() => {
        if (pageModule.handleIgnoreFirstIt()) {
          connectOrDisConnectModule(true, null, false);
          cy.go("back");
          cy.wait(3000);
        }
      });
    });
  });
});
