/*** Atención de tikcets masivos 1 a muchos ***/
const pageModule = require("../../../../../pages/Modules/module");
const {
  getBodyAttetionTicket,
  getOffice,
  changeOptionsOffice,
  connectOrDisConnectModule,
} = require("../../../../../services/ticket");

const {
  getTotAssemble,
  getTotAssembleJSON_1_for_1,
} = require("../../../../../utils/utilities");

const testsAutomatic = require("../../../../../fixtures/Modules/type_attention_1_for_1.json");

describe("Modulos - Atendiendo tickets 1 por 1 - Manual", function () {
  //Realizar el ciclo
  testsAutomatic.forEach((data) => {
    const IsAutomaticOrManual = false;
    const typeForm = true;
    let { skip, describeName, optionsChange, typeAction } = data;

    //Puede ser automatico o manual
    optionsChange = {
      ...optionsChange,
      webServiceAutoCall: IsAutomaticOrManual,
    };

    if (!skip) return;

    data.itsIteration = getTotAssembleJSON_1_for_1(typeAction);

    const { itsIteration } = data;

    let optionsLogin = {
      isNextStep: 0,
      isLogin: false,
    };
    const user = Cypress.env("users")[0];
    const office = getOffice(user.offices, false);

    describe(describeName, function () {
      before(() => {
        cy.wrap(0).as("enterOnce");
        cy.visit(Cypress.env("pageModule"), { timeout: 15000 });
        cy.wrap(changeOptionsOffice(office.id, optionsChange));
        cy.wait(2500);
        //aqui no tratar de que busque la oficina solo el logue al paso 2
        pageModule.handleLoginCompleted(optionsLogin, office, user);
      });

      beforeEach(function () {
        cy.wrap({
          isMassive: false,
          isCreateTicket: true,
          isGetTicketSkip: false,
          isVideoCall: typeAction === 2 ? true : false,
          typeForm,
        }).as("options");

        cy.wrap(office).as("office");
        if (this.enterOnce === 0) {
          cy.wrap(1).as("enterOnce");
          cy.wrap(localStorage.getItem("user_data")).as("user_data");
          cy.wrap(localStorage.getItem("web-service-offices")).as(
            "web_service_offices"
          );
        } else {
          console.log("*****OFICINA*****");
          optionsLogin = { ...optionsLogin, isLogin: true };
          cy.wrap(optionsLogin).as("optionsLogin");
          pageModule.handleLoginCompleted(optionsLogin, office, user);
          localStorage.setItem("user_data", this.user_data);
          localStorage.setItem("web-service-offices", this.web_service_offices);
        }
      });

      itsIteration.map((itemIt) => {
        const { skip, describeName, tests } = itemIt;

        if (!skip) return;

        data.itsIteration = getTotAssemble([...tests], {
          typeAction,
          IsAutomaticOrManual,
          typeForm
        });

        describe(describeName, function () {
          tests.forEach((test) => {
            const {
              skip,
              nameCommand,
              typeFirstCall,
              command,
              isGetTicketSkip,
            } = test;

            if (!skip) return;
            it(nameCommand, function () {
              cy.log(typeFirstCall + "" + command);

              cy.wrap(
                getBodyAttetionTicket(
                  this.office,
                  { ...this.options, isGetTicketSkip },
                  false
                )
              ).then((data) => {
                pageModule.handleComponentAttentions(
                  { ...data, IsAutomaticOrManual },
                  test
                );
              });
            });
          });
        });
      });

      afterEach(() => {
        if (pageModule.handleIgnoreFirstIt()) {
          connectOrDisConnectModule(true, null, false);
          cy.go("back");
          cy.wait(3000);
        }
      });
    });
  });
});
