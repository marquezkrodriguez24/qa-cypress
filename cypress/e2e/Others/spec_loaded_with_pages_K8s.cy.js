import pageModule from "../../pages/Modules/module";
import pageLoginLocalWeb from "../../pages/LocalWeb/login";
import {
  getOffice,
  changeOptionsOffice,
  getBodyAttetionTicket,
} from "../../services/ticket";

const validationHomeLocalWeb = (user) => {
  cy.wait(3000);
  cy.get(".breakpoint__desktop-only h3").should(
    "have.contain",
    "Inicia Sesión en ZeroQ"
  );

  cy.get(".breakpoint__desktop-only p")
    .eq(0)
    .should("have.contain", "¿Olvidaste tu contraseña? Recupérala aquí");

  cy.get(".breakpoint__desktop-only p")
    .eq(1)
    .should("have.contain", "O también puedes ingresar con:");

  cy.get(".breakpoint__desktop-only p")
    .eq(2)
    .should("have.contain", "No pierdas más tiempo y regístrate.");

  cy.loginLocalWeb(user);

  cy.get(".ant-row h3").should("have.contain", "¿Qué trámite deseas realizar?");
};

describe("Cargas de páginas K8s", function () {
  beforeEach(() => {
    const optionsLogin = {
      isNextStep: 0,
      isLogin: false,
      
    };

    const user = Cypress.env("users")[0];
    const office = getOffice(user.offices, false);
    cy.wrap(optionsLogin).as("optionsLogin");
    cy.wrap(Cypress.env("times").callTicket).as("timeOut");
    cy.wrap(office).as("office");
    cy.wrap(user).as("user");

    if (
      Cypress.mocha.getRunner().suite.ctx.currentTest.title ===
      "Modulo Web Atendiendo tickets"
    ) {
      const optionsChange = {
        webServiceAutoCall: false,
        attention_arrived: false,
        client_type: false,
      };
      cy.wrap(changeOptionsOffice(office.id, optionsChange));
      cy.wait(3000);

      cy.visit(Cypress.env("pageModule"));
      pageModule.handleLoginCompleted(optionsLogin, office, user);
    }
  });

  it("Web app", function () {
    cy.visit(`${Cypress.env("pageLocalWeb")}/signin`);
    validationHomeLocalWeb(this.user);
    cy.wait(10000);
    pageLoginLocalWeb.handleLogout();
  });

  it("Panel de Control", function () {
    const { nameOffice, slug } = this.office;

    //1.Iniciamos en el localWEB
    cy.visit(`${Cypress.env("pageLocalWeb")}/signin`);
    validationHomeLocalWeb(this.user);

    //2.Nos vamos al panel ya ingresado desde el localWEB
    cy.visit(`${Cypress.env("pageLocalWeb")}/control/#/office/${slug}`);
    cy.wait(3000);
    cy.get("header h1").should("have.contain", nameOffice);
    cy.get(".ant-dropdown-trigger.ant-dropdown-link").should(
      "have.contain",
      this.user.email
    );

    //3.Regresamos al panel para cerrar sesión
    cy.visit(`${Cypress.env("pageLocalWeb")}`);
    cy.wait(10000);
    pageLoginLocalWeb.handleLogout();
  });

  it("Kiosko web", function () {
    //De momento quemado el officeId, kioskId y token
    cy.visit(
      `${Cypress.env(
        "pageLocalWeb"
      )}/kiosk?officeId=1777&kioskId=13169&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9`
    );
    cy.wait(3000);
    cy.get("#root h2").eq(0).should("have.text", "Espera un momento");
    cy.get("#root h2").eq(1).should("have.text", "Te atenderemos pronto");
    cy.get(".waiting-text")
      .eq(0)
      .should(
        "have.text",
        "Nuestros ejecutivos se encuentran en otra atención."
      );
    cy.get(".waiting-text")
      .eq(1)
      .should(
        "have.text",
        "Revisa tu número de ticket en la pantalla principal."
      );
  });

  it("Botonera web", function () {
    //De momento quemado el slug, pedir que se habiliten en mi oficina
    cy.visit(`${Cypress.env("pageLocalWeb")}/botonera/demo-web-frontend`);
    cy.wait(3000);
    cy.get(".foreign-container button").should(
      "have.contain",
      "Continuar sin RUT"
    );

    cy.get(".keypad-screen").should("have.contain", "INGRESE SU RUT");

    cy.get(".continue-button")
      .eq(1)
      .should("contain.text", "Continuar")
      .and("have.css", "background-color", "rgb(140, 140, 140)");

    for (let i = 0; i < 9; i++) {
      cy.get(".btn-pad").eq(8).click();
    }

    cy.get(".keypad-screen").should("have.contain", "99999999-9");

    cy.get(".continue-button")
      .eq(1)
      .should("contain.text", "Continuar")
      .and("have.css", "background-color", "rgb(0, 80, 179)");
  });

  it("Display web", function () {
    const { id } = this.office;
    cy.visit(`${Cypress.env("pageLocalWeb")}/display/${id}`);
    cy.wait(3000);
    cy.get("#root p").eq(0).should("have.text", "Ticket");
    cy.get("#root p").eq(1).should("have.text", "Módulo");
    cy.get("#onDemandSinVideo div")
      .eq(2)
      .find("span")
      .should("have.text", "Últimos llamados");
  });

  it("Command web", function () {
    //1.Iniciamos en el localWEB
    cy.visit(`${Cypress.env("pageLocalWeb")}/signin`);
    validationHomeLocalWeb(this.user);

    cy.visit(`${Cypress.env("pageLocalWeb")}/command/#/`);
    cy.wait(3000);

    cy.get(".contact-item span")
      .eq(0)
      .should("have.contain", "Mesa de ayuda: soporte@zeroq.cl");
    cy.get(".contact-item span").eq(1).should("have.contain", "+56 2 26665563");

    cy.get(".office.online h2").eq(0).should("have.text", "Kevin QA");
    cy.get(".office.online h2")
      .eq(1)
      .should("have.text", "Demo web - Frontend");

    cy.visit(`${Cypress.env("pageLocalWeb")}`);
    cy.wait(10000);
    pageLoginLocalWeb.handleLogout();
  });

  it("Stats web", function () {
    //1.Iniciamos en el localWEB
    const { email } = this.user;

    cy.visit(`${Cypress.env("pageLocalWeb")}/signin`);
    validationHomeLocalWeb(this.user);

    cy.visit(`${Cypress.env("pageLocalWeb")}/stats`);
    cy.wait(3000);

    cy.get("#ok")
      // .should("have.css", "background", "rgba(9, 53, 90, 0.89)")
      .click();

    cy.get(`.ant-menu-submenu-title[title="${email}"]`).should(
      "have.text",
      email
    );

    cy.get(`.ant-col-md-18.ant-col-lg-18 h2`).should(
      "have.text",
      "Vista General"
    );

    cy.visit(`${Cypress.env("pageLocalWeb")}`);
    cy.wait(10000);
    pageLoginLocalWeb.handleLogout();
  });

  it("Requested Forms API", function () {
    //De momento quemada la Idoficina y el Idline

    cy.request(
      "GET",
      `${Cypress.env(
        "hostReserve"
      )}/forms/config/office/1777/line/12307/current`
    ).then((response) => {
      expect(response.body).to.have.property(
        "created_by",
        "hpichardo@zeroq.cl"
      );
      expect(response.body).to.have.property("lineId", 12307);
      expect(response.body).to.have.property("officeId", 1777);
    });
  });
});
