const getTotAssemble = (data, options) => {
  /** Si typeAction es:
   * 0: Atención estandar si opciones activadas
   * 1: Atención boton llego / tipo cliente
   * 2: Atención video llamada meet:true
   */
  const { typeAction, IsAutomaticOrManual, typeForm } = options;
  const enumCallSkip = ["LT", "LUT"];
  let typeFirstCall = "";

  function joinCommand(command, typeFirstCall) {
    //Si el search es R, el primer llamado se pone despues de la R, ya que esta en la segunda posicion,
    //Por que este comando tiene primer llamado a diferencia del (CallSkip)
    const arrays = command.split("");
    arrays.splice(command.search("R") >= 0 ? 2 : 1, 0, typeFirstCall);
    return arrays.join("");
  }

  data.map((test) => {
    const { command } = test;

    let isFinishAttention =
      command.search("D") >= 0 || command.search("N") >= 0 ? true : false;

    test.isDerivate = command === "D" ? true : false;
    test.isGetTicketSkip = false;

    if (IsAutomaticOrManual) {
      //Definimo el primer llamado, no todos los IT lo llevan
      /**
       * A: Boton llego
       * M: Boton llamar, cuando se llama los ticket manuales en las oficinas
       */
      typeFirstCall =
        typeAction === 1 && command !== "RS"
          ? "A"
          : typeAction === 2
          ? "A"
          : "";

      isFinishAttention =
        isFinishAttention && command.search("T") >= 0 ? true : false;
    } else {
      typeFirstCall =
        typeAction === 0 && command !== command.search("L") < 0
          ? "M"
          : typeAction === 0 && command === command.search("L") >= 0
          ? ""
          : (typeAction === 1 || typeAction === 2) && command.search("L") < 0
          ? "MA"
          : "A";

      isFinishAttention =
        isFinishAttention &&
        (command.search("T") >= 0 ||
          command.search("S") >= 0 ||
          command.search("P") >= 0)
          ? true
          : false;
    }
    //Agregar la el comando de validar formulario
    typeFirstCall = typeForm ? `${typeFirstCall}O` : typeFirstCall;

    if (isFinishAttention) test.command = joinCommand(command, typeFirstCall);

    //Si existe en el enumCallSkip, el typeFirstCall se le agrega al command
    if (enumCallSkip.filter((element) => element === command).length > 0) {
      /*Si automatico se le agrega a todos los LT
      Si es manual se le agrega a los boton llego / video llamada*/
      if (
        IsAutomaticOrManual ||
        (!IsAutomaticOrManual && (typeAction === 1 || typeAction === 2))
      ) {
        test.command = joinCommand(command, typeFirstCall);
      }

      //Se setea para que no haga el primer llamado
      typeFirstCall = "";
      test.isGetTicketSkip = true;
    }

    test.typeFirstCall = typeFirstCall;
  });

  return data;
};

const getTotAssembleJSON_1_for_1 = (typeAction) => {
  function getStandar(commandFather) {
    const enumCallSkip = ["LT", "LUT"];

    let commands = [
      { nameCommand: "Terminar Atención", command: "T", skip: true },
      { nameCommand: "Saltar Atención", command: "S", skip: true },
      { nameCommand: "Llamar Saltado", command: "LT", skip: true },
      { nameCommand: "Pausar Módulo", command: "P", skip: true },
      //{ nameCommand: "Llamado Futuro", command: "F", skip: true },
    ];

    commands = commands.map((item, idx) => {
      //Si es typeAction === 2 es porque es video llamada, por lo cual
      //U: equivale al boton Unirse
      if (idx === 0) {
        item.command = typeAction === 2 ? "UT" : "T";
      } else if (idx === 2) {
        item.command = typeAction === 2 ? "LUT" : "LT";
      }

      //Si es Derivar y llamado no se agrega el comando padre y el primer tipo primer llamado
      item.command = !enumCallSkip.find((element) => element === item.command)
        ? `${commandFather}${item.command}`
        : item.command;

      return item;
    });

    return commands;
  }

  const testsStandar = [
    {
      describeName: "Estandar",
      skip: true,
      tests: getStandar(""),
    },
    {
      describeName: "Derivar Número",
      skip: true,
      tests: getStandar("D"),
    },
    {
      describeName: "Derivar Número - Pausar",
      skip: true,
      tests: getStandar("N"),
    },
    {
      describeName: "Rellamar",
      skip: true,
      tests: getStandar("R"),
    },
    // {
    //   describeName: "Rellamar/Derivar Número",
    //   skip: false,
    //   tests: getStandar("RD"),
    // },
    // {
    //   describeName: "Rellamar/Derivar Número - Pausar",
    //   skip: false,
    //   tests: getStandar("RN"),
    // },
  ];

  return testsStandar;
};

const getNumberRandom = (amount) => {
  return Math.floor(Math.random() * amount);
};

module.exports = {
  getTotAssemble,
  getTotAssembleJSON_1_for_1,
  getNumberRandom,
};
