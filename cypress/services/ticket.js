import { query, post, patch } from "./API_Requests";
import { getNumberRandom } from "../utils/utilities";

/**
 *
 * @param {*} offices Offinas que tiene acceso el usuario
 * @param {*} isModuleOrSuper valor booleano para identificar si es modulo(false) o super modulo(true)
 */

const getOffice = (offices, isModuleOrSuper) => {
  return offices.filter(({ typeModule }) => typeModule === isModuleOrSuper)[0];
};

const getFormatFilas = (filas) => {
  return filas.map(({ prefix, name, id, attention_reasons, slug, type }) => {
    //Verificamos si tiene foldel el motivo
    const amount_folders = attention_reasons
      ? attention_reasons.filter((reason) => {
          if (reason.folders) {
            if (reason.folders.length > 0) return reason;
          }
        }).length
      : 0;

    return {
      id,
      prefix,
      name,
      attention_reasons: {
        reason_v2: amount_folders > 0 ? true : false,
        amount_reason:
          amount_folders > 0
            ? amount_folders
            : attention_reasons
            ? attention_reasons.length
            : 0,
      },
      slug,
      type,
    };
  });
};

const getFilasOfficeID = async (office) => {
  const { id: officeId } = office;
  const { data } = await query(`/v2/offices/${officeId}/lines`, {
    versionAPI: 0,
  });

  return data;
};

const getAssigns = async (office) => {
  const { id: officeId } = office;
  const { data } = await query(`/v1/offices/${officeId}/devices/assigns`, {
    versionAPI: 0,
  });
  return data;
};

/**
 * Funcion con el proposito de traer los modulos y filas de una oficina
 * @param {*} office Offina a la cual se conectara
 */
const getModulesOrFilas = async (office) => {
  const { id: officeId, nameModule, typeModule } = office;

  //Traemos todos lo modulos, filas de la oficina con su respectiva asignaciones
  let modules = await query(`/turn-o-matic/offices/${officeId}/modulos`, {
    versionAPI: 1,
  });

  //Sacamos el modulo el cual se contectara
  let moduleFound = modules.filter(({ name }) => name === nameModule)[0];
  const { id: moduleId, name, status } = moduleFound;

  let filasOffice = await getFilasOfficeID(office);
  filasOffice = !typeModule
    ? filasOffice.filter(({ type }) => type !== "kiosk")
    : filasOffice.filter(({ type }) => type === "kiosk");

  const assigns = await getAssigns(office);
  let filas = getFormatFilas(
    //Si es super modulo se toma todas las filas del mismo, ya que no podemos filter por ID del X modulo
    await (!typeModule ? getFilasModuleID(officeId, moduleId) : filasOffice)
  );

  //Actualizamos los datos de la oficina con el modulo que se conecto
  office.nameModule = name;
  office.moduleConected = { officeId, moduleId };

  //return

  //Asignamos la filas a su respectivo modulo
  modules = modules
    .map(({ id, name }) => {
      const filasModule = assigns
        //Sacamos de de todas las filas de la oficinas, las filas del modulo que se conecto
        .filter(
          ({ line_id }) => filas.filter(({ id }) => line_id === id).length === 0
        )
        //Sacamos todos los id asociado al modulo
        .filter(({ device_id }) => device_id === id)
        .map(({ line_id }) => {
          return getFormatFilas(
            //Sacamos el objeto de la fila para almacenarlo en el modulo
            filasOffice.filter(({ id }) => id === line_id)
          )[0];
        });

      //Formateamos la data del modulo original, a la data que tenemos
      return {
        ...office,
        nameModule: name,
        filasModule,
      };
    })
    .filter(({ nameModule }) => nameModule !== name);

  //Nota: Validar que los modulos que se manden tengan filas asignadas

  return {
    modules: [modules[getNumberRandom(modules.length)]],
    filas: typeModule ? filas.filter(({ type }) => type === "kiosk") : filas,
    status,
  };
};

/**
 *
 * @param {*} officeId Identificador de la Oficina seleccionada
 * @param {*} moduleId Identificador del Modulo el cual se ingreso
 */
const getFilasModuleID = async (officeId, moduleId) => {
  const filas = await query(
    `/turn-o-matic/offices/${officeId}/modulos/${moduleId}/lines`,
    { versionAPI: 1 }
  );
  return filas.filter(({ type }) => type !== "kiosk");
};

const createTickets = async (data, options) => {
  let repeatTicket = 0;

  const { isMassive, isCreateTicket, isVideoCall, typeForm } = options;
  let { filas } = data;

  data.filas_tickets = [...filas];

  /**Si no es masivo se crea 1 ticket a 1 fila,
   * en caso contrario muchos ticket a muchas filas */
  if (!isMassive && isCreateTicket) {
    let number = Math.floor(Math.random() * filas.length);
    data.filas_tickets = [filas[number]];
    data.ticketAmount = 1; // se setea ya que solo sera 1 ticket por proceso
  }

  const {
    office: {
      moduleConected: { officeId },
      ticketAmount,
      filasNumberAttention,
      //typeModule,
    },
    filas_tickets,
  } = data;

  /*Si la variable de entorno es mayor le asignamos 
  la cantidad de filas que tiene el modulo*/
  let amountFilas = filasNumberAttention;
  //Si es mayor el # indicado de las filas asignada al modulo,
  //se asigna el total de filas que vienen por el API
  if (amountFilas > filas_tickets.length) amountFilas = filas_tickets.length;

  filas_tickets.length = amountFilas;

  for (let i = 0; i < filas_tickets.length; i++) {
    let dataCreateTicket = { line_id: filas_tickets[i].id };

    if (isVideoCall) dataCreateTicket.meet = true;
    /*Validamo si la creacion del ticket es tipo FORM */
    if (typeForm) {
      dataCreateTicket.meta = {
        forms: {
          questions: Cypress.env("questionsForm"),
          type: "office",
        },
      };
    }

    for (let i = 0; i < ticketAmount; i++) {
      const { status } = await post(
        `/turn-o-matic/offices/${officeId}/tickets/new`,
        dataCreateTicket,
        { versionAPI: 1 }
      );
      if (status === 200) repeatTicket++;
    }
  }

  return repeatTicket;
};

const getTickets = async (url, office) => {
  const {
    moduleConected: { officeId: id },
    typeModule,
  } = office;
  const tickets = await query(url, { versionAPI: 1 });

  const numberTickets = !typeModule
    ? tickets.length
    : tickets
        .filter(({ officeId }) => officeId === id)
        .map(({ nextAttentions }) =>
          nextAttentions.length > 0 ? nextAttentions.length : 0
        )
        .reduce((a, b) => a + b, 0);

  return { tickets, numberTickets };
};

const getBodyAttetionTicket = async (office, options) => {
  const { isCreateTicket, isGetTicketSkip } = options;
  let url = "";

  //Sacamos los modulos y filas
  const data = await getModulesOrFilas(office);

  const {
    moduleConected: { officeId, moduleId },
    typeModule,
  } = office;
  data.office = office;

  /**Verificamos si el TEST es para el modulo o el superModulo */
  if (!typeModule) {
    //Si no es llamado saltado, podemos crear y consultar los ticket de X modulo
    if (!isGetTicketSkip) {
      //Creamos el ticket si es necesario

      if (isCreateTicket) await createTickets(data, options);

      url = `/turn-o-matic/offices/${officeId}/modulos/${moduleId}/tickets`;
    } else {
      //Consulta de los ticket saltados
      url = `/turn-o-matic/offices/${officeId}/modulos/${moduleId}/tickets/skipped`;
    }
  } else {
    if (isCreateTicket) await createTickets(data, options);
    url = `/turn-o-matic/supermodulo/tickets`;
  }

  const { tickets, numberTickets } = await getTickets(url, office);

  data.numberTickets = numberTickets;
  data.tickets = tickets;
  data.options = options;
  data.questionsForm = Cypress.env("questionsForm");

  console.log("data General", data);

  return data;
};

const getAllFilasWithBlocks = async (slug) => {
  let all_filas = await query(`/v1/state/${slug}/lines`, { versionAPI: 0 });

  const filas_block = await query(
    `/reservations/api/v2/offices/${slug}/settings/`,
    {
      versionAPI: 2,
      typeAuth: "R",
    }
  );

  all_filas = Object.keys(all_filas)
    .map((slug) => {
      return filas_block.configByLine.filter(({ _id }) => _id === slug).length >
        0
        ? all_filas[slug]
        : null;
    })
    .filter((item) => {
      if (item) return item;
    });

  return all_filas;
};

const getOfficeStorage = (office) => {
  const { id: officeId } = office;
  return JSON.parse(localStorage.getItem("web-service-offices")).data.filter(
    ({ id }) => id === officeId
  )[0];
};

const changeOptionsOffice = async (officeId, optionsChange) => {
  let data = {
    office: {
      options: optionsChange,
    },
  };

  return await patch(`/v1/offices/${officeId}`, data, {
    versionAPI: 0,
    typeAuth: "V",
  });
};

/**
 *
 * @param {*} isForEach si es true es por que se activa en los afterEach de los test
 * @param {*} office  objeto que me indica el modulo y oficina logueada
 * @param {*} type Si es true = inicio en modulo, false = salir del modulo
 */

const connectOrDisConnectModule = async (
  isForEach = false,
  moduleConected,
  type
) => {
  if (isForEach) {
    moduleConected = {
      officeId: JSON.parse(localStorage.getItem("officeId")),
      moduleId: JSON.parse(localStorage.getItem("moduleId")),
    };
  }

  const { officeId, moduleId } = moduleConected;
  if (!officeId || !moduleId) return null;

  const url = type
    ? `/turn-o-matic/offices/${officeId}/modulos/${moduleId}/session`
    : `/turn-o-matic/delete/offices/${officeId}/modulos/${moduleId}/session`;
  //Iniciamos sesion
  return await post(url, {}, { versionAPI: 1 });
};

const attendTicket = async (moduleConected, ticket) => {
  const {
    id,
    line: { id: lineId },
  } = ticket;
  const { officeId, moduleId } = moduleConected;

  let data = { id, modulo_id: moduleId, type: "ticket" };

  await connectOrDisConnectModule(false, moduleConected, true);

  //Atendemos el ticket
  const response = await post(
    `/turn-o-matic/offices/${officeId}/lines/${lineId}/attentions?`,
    data,
    { versionAPI: 1, typeAuth: "A" }
  );

  await connectOrDisConnectModule(false, moduleConected, false);

  return response;
};

const getElapse = async (officeId) => {
  //Atendemos el ticket
  const response = await query(
    `/turn-o-matic/offices/${officeId}/cache/keys?key=turn-o-matic.state.offices.${officeId}.attentions`,
    { versionAPI: 1, typeAuth: "C" }
  );

  return response;
};

module.exports = {
  createTickets,
  attendTicket,
  changeOptionsOffice,
  getModulesOrFilas,
  connectOrDisConnectModule,
  getOffice,
  getTickets,
  getBodyAttetionTicket,
  getAllFilasWithBlocks,
  getOfficeStorage,
  getElapse,
};
