const requestFetch = (urlRelative, options, actions) => {
  const { versionAPI, typeAuth = "L" } = actions;
  const headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
  };
  /*typeAuth define el token para realizar los request si es:
    L: proviene del localStorage sesion del proyecto modulo
    A: proviene de otro proyecto (No se cual :( )
    C: proviene del proyecto command,
    V: proviene del API V1 no se cual proyecto
    R: Proviene del API Rerserva
  */

  const token =
    typeAuth === "L"
      ? `Bearer ${JSON.parse(localStorage.getItem("user_data")).token}`
      : typeAuth === "A"
      ? `Bearer ${Cypress.env("authAPI_ATT")}`
      : typeAuth === "C"
      ? `Bearer ${Cypress.env("authAPI_COMMAND")}`
      : typeAuth === "R"
      ? `${Cypress.env("authAPI_Reservation")}`
      : Cypress.env("authAPI_V1");

  headers["Authorization"] = `${token}`;

  //this.domain + urlRelative = a futuro sabiendo bien las URL
  /* Si el parametro versionAPI
    0: Version 1 / 1: Version 2 / 1: Version Reserva
  */
  const domain =
    versionAPI === 0
      ? Cypress.env("hostV1")
      : versionAPI === 1
      ? Cypress.env("hostV2")
      : Cypress.env("hostReserve");

  // console.log(`${domain}${urlRelative}`);

  return fetch(`${domain}${urlRelative}`, {
    headers,
    ...options,
  })
    .then((response) => response.json())
    .catch((error) => Promise.reject(error));
};

const query = (url, actions) => {
  return requestFetch(
    url,
    {
      method: "GET",
    },
    actions
  ).catch(() => {
    console.log("Error QUERY");
  });
};

const post = (url, data, actions) => {
  return requestFetch(
    url,
    {
      method: "POST",
      body: JSON.stringify(data),
    },
    actions
  ).catch(() => {
    console.log("Error POST");
  });
};

const patch = (url, data, actions) => {
  return requestFetch(
    url,
    {
      method: "PATCH",
      body: JSON.stringify(data),
    },
    actions
  ).catch(() => {
    console.log("Error PATCH");
  });
};
module.exports = { query, post, patch };
